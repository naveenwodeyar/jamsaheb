FROM openjdk:21

RUN mkdir /usr/app/

COPY target/Yogi_App.jar /usr/app/

WORKDIR /usr/app/

ENTRYPOINT ["java","-jar","Yogi_App.jar"]
